# Django API framework

This package provides bindings between generic [django](https://www.djangoproject.com/) views and [OpenAPI](https://www.openapis.org) specification files (using the [apigen](https://gitlab.com/fabian_kreutz/apigen) tool).
Use it to

* import generic view classes for easy django server implementation
* generate an OpenAPI specification out of the implementation
* generate a server stub implementation from an OpenAPI spec
* validate an implementation against a specification

## API implementation

Djangoapi supports one class per operation (= http method + path).
Input parameters are defined using pydantic BaseModels.
Some more supporting classes are instantiated with schema-related extra information.

A rather comprehensive example shows the intent:

```yaml
openapi: 3.0.1
info:
  title: Example API
  version: 0.0.1
paths:
  /go/:
    get:
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - name
              properties:
                name:
                  type: string
                value:
                  type: integer
      responses:
        401:
          description: "Authentication required"
        400:
          description: "Bad Request"
        415:
          description: "Unsupported media type"
        200:
          description: Success
          content:
            application/json:
              schema:
                title: ID
                type: integer
```

will produce

```py
from djangoapi.apibase import (
    accept,
    ApiInput,
    ContinueProcessing,
    ApiEndpoint,
    ApiOperation,
    PathItem,
)
from pydantic import BaseModel
from typing import Optional


class EapiGetGo(ApiOperation):
    class Meta:
        operationId = "EAPI-get-/go/"

    class BodyInput(BaseModel):
        class Config:
            extra = "allow"

        name: str
        value: Optional[int]

    def authenticate(
        self, request, body: BodyInput
    ) -> None | ContinueProcessing:
        """Authentication required"""
        return ContinueProcessing()

    def process(self, request, body: BodyInput) -> int:
        """Success"""
        return 1

    __pipeline = [
        (authenticate, 401),
        (
            accept("application/json", description="Unsupported media type"),
            415,
        ),
        (ApiInput("Bad Request", body=BodyInput), 400),
        (process, 200),
    ]


api = ApiEndpoint(
    title="Example API",
    version="0.0.1",
    paths={"/go/": PathItem(get=EapiGetGo)},
)
```

### Pipeline logic

When the operation is called, the dispatcher will look for the pipeline.

#### Inheriting the pipeline

It is defined as a python "private" variable to limit its scope to the current class.
This allows the logic to concatenate pipelines of superclasses:

```
class Authenticator(ApiOperation):
    def authenticate(self, request):
        pass

    __pipeline = [(authenticate, 401)]

class MyOperation(Authenticator):
    def process(self, request):
        pass

    __pipeline = [(process, 200)]
```

This will first call the authenticate method and only the proceed to the process function.

#### Working through the pipeline

Each element of the pipeline is a tuple of a callable and an HTTP response code.
The callable will either return a `ContinueProcessing` object (which is basically a dictionary)
to jump to the next item of the pipeline, or raise an Exception (resulting in an automatic 500
response), or return any other object, which will be serialized to form the response together
with the HTTP response code associated with the callable.

The callable will usually be either

* a provided helper function like
  * `accept`, which checks the request body's mediatype and fails if it does not match the given options
  * `ApiInput`, a class that processes headers, query string or body; it can be subclassed,
* a function previously defined in the same class (that's why the pipeline is defined last)
* any globally addressable function.

These callables will receive the currently active operation instance and the django request object
as parameters plus any other value that was previously given in a `ContinueProcessing` response.

Note, that an `ApiInput` processing step will add the parameters *header*, *body* and *query* into the
parameter dict, respectively if they are defined as input sources.

A small example might illuminate this:

```python
def external_function(operation, request, body):
    return ContinueProcessing(user=User.objects.get(id=body.id))

class MyOperation:
  class InputBody(BaseModel):
    id: int

  def internal_function(self, user, **_):
    return f"Success for {user.name}"

  __pipeline = [
    (ApiInput(body=InputBody), 400),
    (external_function, 417),
    (internal_function, 200),
  ]
```

Here the first pipeline step will check the request body and parse it into an `InputBody` dataclass.
This is added as *body* into the parameter list.

The next pipeline step is `external_function`, which accesses this body and returns a `ContinueProcessing`
object containing the user.  This also is added to the parameter list.

Finally the internal function is called with the *request*, *body* and *user*, but as we are not interested in the former two, we ignore them with the unnamed keywords parameter specification.
As this function does not return a `ContinueProcessing` object as the other two, the processing
ends here and the appropriate HTTP response of 200 (associated with `internal_function`) is returned with a rendition of the returned string.

Note, that due to the internal consistency of python methods, both the external and the internal
function are called with the operation as first parameter.

#### Error responses

Error responses should be rather uniform, even if there are many different scenarios that trigger them.
For this reason, the ApiOperation defines an `ErrorResponse` class and has a function `err`, which
creates a `self.ErrorResponse` object from its parameters.

The scenarios are

* a helper function (accept, ApiInput) wants to return an error
  * as even external functions receive the operation instance as parameter, they need only to call
    `operation.err("error description")` and return its response
* a pipeline callable raises an exception or the last element returns a `ContinueProcessing` (a.k.a. dropoff)
  * these also call the operation's `err()` function and return its response
* any custom function should also use `self.err()` and, if return annotations are used, specify `ApiOperation.ErrorResponse` or (if the class redefines this class) simply `ErrorResponse` as type hint.

The translation between spec and code tries to normalize error responses to this conventionally named subclass.
The base `ApiOperation.ErrorResponse` is a *pydantic.BaseModel* with two attributes: a string *result* with the fixed value of "error" and a dictionary *errors* which contains the parameter given to the `.err()` function.

```python
class ApiOperation(View):

    class ErrorResponse(BaseModel):
        errors: dict[str, Any]
        result: str = "error"

        def __init__(self, errors="?", **kwargs):
            if isinstance(errors, str):
                errors = {"processing": errors}
            kwargs["errors"] = errors
            super().__init__(**kwargs)

    def err(self, data):
        return self.ErrorResponse(data)
```

#### Parsing input

Input can be given through

* path parameters
* query or header parameters
* the request body.

While the first (path) parameters are given directly as keyword arguments (like in django views),
the other types of input are defined through data classes and bound to the pipeline via the
`ApiInput` object.

Another helper function `accept` can be used to check for content types.

```py
class MyOperation(ApiOperation):
  class HeaderInput:
    version: int

  class QueryInput:
    name: str

  class BodyInput:
    class Config:
      extra: "forbid"
    color: str

  def process(self, request, userId, header, query, body):
    return {"result": (
        f"User {userId} requested {query.name} "
        f"in v{header.version} in color {body.color}")}

  __pipeline = [
    (accept("application/json"), 415),
    (ApiInput(header=HeaderInput, query=QueryInput, body=BodyInput), 400),
    (process, 200),
  ]

api = ApiEndpoint(..., path={"/user/<int:userId>/": PathItem(post=MyOperation)})
```

In the above example, the request to provide these inputs would look like this:

```
POST /api/user/123/?name=Bob http/1.1
Host: server.test
Content-Type: application/json
Content-Length: 17
X-version: 0.4

{"color": "blue"}
```

## Django specifics

As it turns out, preciously little integration with Django is needed.
It is however a dependency, so that the api endpoint can generate a list of urlpatterns
referencing views.

To make it work with django you need to define your api endpoint:

```py
# myapi.py

class MyOperation(ApiOperation):
    pass

api = ApiEndpoint(
    title="Example API",
    version="1.2.3",
    paths={"/go/": PathItem(get=MyOperation)})
```

This api object can then generate the urls for the django router:

```py
# urls.py

from django.urls import path
from myapi import api

urlpatterns = [
    path('api/', api.urls),
]
```

Please note that [django](https://docs.djangoproject.com/en/4.1/topics/http/urls/) and [OpenAPI](https://swagger.io/specification/#definitions) use different conventions for **path templating**.

Using djangoapi, the urls will be translated from the OpenAPI `/user/{userId}/` to django's `user/<int:userId>/`, using the schema type definition of the path parameters object.
