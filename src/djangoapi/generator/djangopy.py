'''
Note: generators can be complex to any extend. This implementation tries to
give it a good shot but will fail on some esoteric cases.

As it is now it is not useful to generate actually "working" code. Instead it
gives a good starting template which then must be manually adapted.

In particular:

* apibase encourages the use of a standard "ErrorResponse" class, which is
  also returned by the ApiOperation.err function, which in turn is used by
  ApiInput classes and other functional helpers.
  If now the spec defines several different error responses as the content
  of these HTTP returns, or even combinatoric return values, then the
  generated code will not accurately reproduce this logic.
'''
from dataclasses import dataclass
from itertools import groupby
from operator import attrgetter
from pydantic import BaseModel
from typing import Any
from apigen.generators import BaseGenerator
from apigen.spec import openapi
from djangoapi.apibase import ApiOperation
from djangoapi.codegen.elements import (
    Assignment, FunctionCall, Reference,
    MethodDefinition, ClassDefinition, CodeFile)
from djangoapi.codegen.schema import (
    get_mime_schema, get_primitive_type, get_object_subtype,
    make_field_assignment, slugify)


def opId(acro, path, method, operation):
    return operation.operationId or f"{acro}-{method}-{path}"


def compact(**kwargs):
    return {
        key: value.dict() if isinstance(value, BaseModel) else value
        for key, value in kwargs.items()
        if value}


def sort_by_status(code):
    # 401, 403, 415, 4**, 400, 404, 1*, 3*, 2*, 5*
    hundredth = code // 100
    if hundredth == 4:
        try:
            return [401, 403, 415, 0, 400, 404].index(code)
        except ValueError:
            return 3
    cat = [4, 1, 3, 2, 5].index(hundredth)
    return code + (cat - hundredth) * 100


def objects_first(schema):
    if schema.type == "object":
        return 0
    return 1


def funcname_for_status_code(code):
    if 400 <= code < 500:
        return {
            401: "authenticate",
            403: "authorize",
            404: "get_object",
            415: "supported_mediatypes"}.get(code, "precondition")
    if 200 <= code < 400:
        return "process"
    if 500 <= code:
        return "on_error"
    return "handle"


def example_value(schema, name=None):
    example = schema.make_example(minimal=True)
    if name and isinstance(example, dict):
        args = ", ".join(f"{key}={value!r}" for key, value in example.items())
        return f"{name}({args})"
    else:
        return repr(example)


def render_path_to_route(path, item):
    first_op = list(item.operations.values())[0]
    format_dict = {
        pp.name: f"<{get_primitive_type(None, pp.schema_)}:{pp.name}>"
        for pp in item.parameters or first_op.parameters
        if pp.in_ == openapi.ParameterLocation.path}
    if format_dict:
        path = path.format(**format_dict)
    return path


def are_schemas_equal(schema1, schema2):
    if schema2 is None:
        return False
    return schema1 == schema2


@dataclass
class Operation:
    code_file: CodeFile
    class_def: ClassDefinition
    error_schema: dict[str, Any] | None = None


@dataclass
class Response:
    operation: Operation
    code: int
    func_name: str
    return_schema: Any

    def __init__(
            self, operation, code, func_name,
            content=None, return_schema=None):
        self.operation = operation
        self.code = code
        self.func_name = func_name
        self.return_schema = return_schema
        if content:
            rt = list(content.values())
            self.return_schema = rt[0].schema_


class PythonServerGenerator(BaseGenerator):
    target_suffix = "py"

    def __init__(self, **kwargs):
        self.code_file = CodeFile()
        self.default_error_schema = \
            ApiOperation.ErrorResponse.schema()["properties"]

    def render(self, spec):
        if spec.type != "openapi":
            raise ValueError(
                "Django server can be generated only from OpenAPI spec")
        self.parse(spec)
        return self.code_file.render()

    def parse(self, spec: openapi.OpenApiV3Spec):
        for tag in spec.tags:
            self.code_file.append(self.make_tag_def(tag))
        for ep_name, ep_data in spec.paths.items():
            for op_name, op_data in ep_data.operations.items():
                self.code_file.append(self.make_operation_class_definition(
                    spec.acronym, ep_name, op_name, op_data))
        self.code_file.append(self.make_api_endpoint(spec))

    def make_tag_def(self, tag: openapi.TagObject):
        tag_varname = tag.name.lower() + "Tag"
        return Assignment(tag_varname, value=FunctionCall(
            self.code_file.get_import("apigen.spec.openapi.TagObject"),
            kwargs=compact(
                name=tag.name,
                description=tag.description,
                external_docs=tag.externalDocs)))

    def make_operation_class_definition(self, acro, path, method, op):
        op_id = opId(acro, path, method, op)
        apiop_cls = self.code_file.get_import("djangoapi.apibase.ApiOperation")
        class_def = ClassDefinition(
            name=slugify(op_id), superclasses=[apiop_cls])
        class_def.append(self.make_operation_metaclass(op_id, op))

        path_params, input_classes = \
            self.make_operation_parameters(class_def, op)
        request_params = [
            "request",
            *path_params,
            *[": ".join(ee) for ee in input_classes.items()]]
        content_types = []
        if op.requestBody:
            content_types = list(op.requestBody.content.keys())

        pipeline = self.make_operation_pipeline(
            class_def, op.responses,
            request_params, input_classes,
            content_types)

        class_def.append(Assignment("__pipeline", value=pipeline))
        return class_def

    def make_operation_pipeline(
            self, class_def, responses, request_params,
            input_classes, content_types):
        cf = self.code_file
        pipeline = []

        op = Operation(code_file=cf, class_def=class_def)
        for code in sorted(responses, key=sort_by_status):
            resp = Response(
                operation=op, code=code,
                func_name=funcname_for_status_code(code),
                content=responses[code].content)
            description = responses[code].description
            if code == 415 and content_types:
                pipe_ref = FunctionCall(
                    cf.get_import("djangoapi.apibase.accept"),
                    content_types, {"description": description})
            elif code == 400:
                pipe_ref = FunctionCall(
                    cf.get_import("djangoapi.apibase.ApiInput"),
                    [description], input_classes)
                if resp.return_schema:
                    self.define_error_response(resp)
            else:
                resp_type, body = self.get_opfunction_body(resp)
                class_def.append(MethodDefinition(
                    resp.func_name, request_params,
                    resp_type, description, body))
                pipe_ref = Reference(resp.func_name)
            pipeline.append((pipe_ref, code))
        return pipeline

    def get_opfunction_body(self, resp):
        cf = resp.operation.code_file
        schema = resp.return_schema
        body = None
        resp_type = None
        if 200 <= resp.code < 400:
            if schema:
                if schema.type == "object":
                    name = schema.title or \
                        f"{resp.func_name.capitalize()}Response"
                    resp_type = get_object_subtype(
                        cf, name, schema, location=resp.operation.class_def)
                    body = "return " + example_value(
                        schema, name=f"self.{resp_type}")
                elif schema.type == "combinator":
                    rtypes = []
                    for opt in sorted(schema.children, key=objects_first):
                        subresp = Response(
                            operation=resp.operation, code=resp.code,
                            func_name=resp.func_name, return_schema=opt)
                        rt, body = self.get_opfunction_body(subresp)
                        rtypes.append(rt)
                    resp_type = " | ".join(rtypes)
                else:
                    resp_type = get_primitive_type(cf, schema)
                    body = "return " + example_value(schema)
            else:
                body = 'return {"result": "Success"}'
        if 400 <= resp.code and schema:
            resp_type = self.define_error_response(resp)
        if body is None:
            cp = cf.get_import(
                "djangoapi.apibase.ContinueProcessing")
            resp_type = f"{resp_type} | {cp}"
            body = f"return {cp}()"
        return resp_type, body

    def define_error_response(self, resp: Response):
        op = resp.operation
        if resp.return_schema.type == "object":
            error_schema = {
                key: value.dict()
                for key, value in resp.return_schema.properties.items()}
        elif resp.return_schema.type == "combinator":
            combined_types = [
                self.define_error_response(Response(
                    operation=resp.operation, code=resp.code,
                    func_name=resp.func_name, return_schema=opt))
                for opt in sorted(
                    resp.return_schema.children, key=objects_first)]
            return " | ".join(combined_types)
        else:
            error_schema = get_primitive_type(
                self.code_file, resp.return_schema)
        if are_schemas_equal(error_schema, op.error_schema):
            return "ErrorResponse"
        elif are_schemas_equal(error_schema, self.default_error_schema):
            return "ApiOperation.ErrorResponse"
        if resp.return_schema.type == "object":
            name = resp.return_schema.title or "ErrorResponse"
            if name == "ErrorResponse" and not op.error_schema:
                op.error_schema = resp.return_schema
            typestr = get_object_subtype(
                self.code_file, name,
                resp.return_schema, location=op.class_def)
        else:
            typestr = get_primitive_type(self.code_file, resp.return_schema)
            if not op.error_schema:
                op.error_schema = typestr
                op.class_def.append(Assignment(
                    "ErrorResponse", value=Reference(typestr)))
        return typestr

    def make_operation_parameters(self, class_def, op):
        cf = self.code_file
        parameters = {
            x.value: list(y)
            for x, y in groupby(op.parameters, attrgetter("in_"))}
        path_params, input_classes = [], {}

        if (data := parameters.get("path")):
            for param in data:
                ptype = get_primitive_type(cf, param.schema_)
                path_params.append(f"{param.name}: {ptype}")

        if (rb := op.requestBody):
            if (schema := get_mime_schema(rb)).type == "object":
                btype = get_object_subtype(
                    cf, schema.title or "BodyInput", schema,
                    location=class_def)
            else:
                btype = get_primitive_type(cf, schema)
            input_classes["body"] = Reference(btype)

        if (data := parameters.get("header")):
            class_def.append(cls := ClassDefinition(
                name="HeaderInput",
                superclasses=[cf.get_import("pydantic.BaseModel")]))
            for param in data:
                cls.append(make_field_assignment(
                    cf, param.name, param.required, get_mime_schema(param)))
            input_classes["header"] = Reference(cls.name)

        if (data := parameters.get("query")):
            class_def.append(cls := ClassDefinition(
                name="QueryInput",
                superclasses=[cf.get_import("pydantic.BaseModel")]))
            for param in data:
                cls.append(make_field_assignment(
                    cf, param.name, param.required, get_mime_schema(param)))
            input_classes["query"] = Reference(cls.name)
        return path_params, input_classes

    def make_operation_metaclass(self, op_id, op):
        cls = ClassDefinition(name="Meta")
        tags = [Reference(tag.lower() + "Tag") for tag in op.tags]
        for key, value in compact(
                operation_id=op_id,
                summary=op.summary,
                description=op.description,
                external_docs=op.externalDocs,
                tags=tags).items():
            cls.append(Assignment(key, value=value))
        return cls

    def make_api_endpoint(self, spec: openapi.OpenApiV3Spec):
        result = compact(
            title=spec.info.title,
            version=spec.info.version,
            terms_of_service=spec.info.termsOfService,
            description=spec.info.description,
            contact=spec.info.contact,
            license=spec.info.license,
            external_docs=spec.externalDocs)
        pi_class = self.code_file.get_import("djangoapi.apibase.PathItem")
        result["paths"] = paths = {}
        for path, ep_data in spec.paths.items():
            route = render_path_to_route(path, ep_data)
            kwargs = compact(
                summary=ep_data.summary,
                description=ep_data.description)
            for method, op in ep_data.operations.items():
                op_id = slugify(opId(spec.acronym, path, method, op))
                kwargs[method] = Reference(op_id)
            paths[route] = FunctionCall(pi_class, kwargs=kwargs)
        # TODO: security
        return Assignment("api", value=FunctionCall(
            self.code_file.get_import("djangoapi.apibase.ApiEndpoint"),
            kwargs=result))
