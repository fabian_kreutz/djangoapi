import logging
import sys


def configure_logger():
    logger = logging.getLogger(__package__)
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.INFO)
    return logger


#: global logger instance
logger: logging.Logger = configure_logger()
