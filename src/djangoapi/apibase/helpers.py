from .operation import ContinueProcessing, PipelineResult


def accept(*content_types, description="Unsupported Media Type"):
    '''
    Add to pipeline as such:
        _pipeline = [(accept("application/json"), 415), (InputForm, 400), ...]
    '''
    def check_content_type(view, request, **kwargs) -> PipelineResult:
        if request.content_type not in content_types:
            error_message = f"Must be one of {', '.join(content_types)}"
            return view.err({"content-type": error_message})
        return ContinueProcessing()

    check_content_type.__doc__ = description
    return check_content_type
