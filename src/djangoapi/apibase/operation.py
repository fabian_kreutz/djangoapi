import json
import typing
from pydantic import BaseModel
from pydantic.error_wrappers import ValidationError, ErrorWrapper
from typing import Union, Callable, Any
from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from .logging import logger


class ContinueProcessing(dict[str, Any]):
    pass


PipelineResult = Union[ContinueProcessing, Any]


class ApiInput():
    def __init__(
            self, description="Bad Request",
            query=None, header=None, body=None):
        self.query_data_cls = query
        self.header_data_cls = header
        self.body_data_cls = body
        self.__doc__ = description

    @property
    def __name__(self):
        for cls in [
                self.query_data_cls,
                self.header_data_cls,
                self.body_data_cls]:
            if name := getattr(cls, "__name__", None):
                return name
        return "Unnamed ApiInput"

    def __call__(self, view, request, **kwargs) -> PipelineResult:
        result: ContinueProcessing = ContinueProcessing()
        try:
            if self.query_data_cls:
                data = self.get_query_data(request, self.query_data_cls)
                result["query"] = self.query_data_cls(**data)
            if self.header_data_cls:
                data = self.get_header_data(request)
                result["header"] = self.header_data_cls(**data)
            if self.body_data_cls:
                data = self.get_body_data(request, self.body_data_cls)
                result["body"] = self.body_data_cls(**data)
        except ValidationError as ve:
            errors: dict[str, Any] = {}
            for item in ve.errors():
                p = errors
                for name in item["loc"][:-1]:
                    p = p.setdefault(str(name), {})
                p[str(item["loc"][-1])] = item["msg"] or item["type"]
            return view.err(errors)
        return result

    def get_query_data(self, request, data_cls):
        return self.get_form_data(request.GET, data_cls, True)

    def get_header_data(self, request):
        return {
            key[2:].lower(): value
            for key, value in request.headers.items()
            if key.startswith("X-")}

    def get_body_data(self, request, data_cls):
        if request.content_type == "application/json":
            try:
                return json.load(request)
            except Exception as exc:
                raise ValidationError([ErrorWrapper(exc, "_body")], data_cls)
        if request.content_type in [
                "application/x-www-form-urlencoded",
                "multipart/form-data"]:
            return self.get_form_data(request.POST, data_cls, False)
        # Unsupported content-type should be a 415 error to be checked before
        # starting to parse the form.  If the developer omits the check, it's
        # a 500 error
        raise ValueError(f"Unsupported content-type: `{request.content_type}`")

    def get_form_data(self, form, data_cls, ignore_extra):
        result = {} if ignore_extra else form.dict()
        for name, field in data_cls.__fields__.items():
            if typing.get_origin(field.outer_type_) is list:
                result[name] = form.getlist(name)
            elif name in form:
                result[name] = form.get(name)
        return result


HttpStatus = Union[int, tuple[int, str]]


class ApiOperation(View):
    route: str = ""
    __pipeline: list[tuple[Callable[..., PipelineResult], HttpStatus]]

    class ErrorResponse(BaseModel):
        errors: dict[str, Any]
        result: str = "error"

        def __init__(self, errors="?", **kwargs):
            if isinstance(errors, str):
                errors = {"processing": errors}
            kwargs["errors"] = errors
            super().__init__(**kwargs)

    def make_response(self, data, status):
        if not data:
            return HttpResponse("", status=status)
        if hasattr(data, "dict"):
            data = data.dict()
        return JsonResponse(data, status=status)

    def err(self, data):
        return self.ErrorResponse(data)

    def get_pipeline(self):
        result = []
        for pclass in reversed(type(self).mro()):
            pvarname = f"_{pclass.__name__}__pipeline"
            if hasattr(self, pvarname):
                result.extend(getattr(self, pvarname))
        return result

    def dispatch(self, request, *args, **kwargs):
        params = {"request": request, **kwargs}
        for func_or_input, status in self.get_pipeline():
            try:
                data = func_or_input(self, **params)
            except Exception:
                logger.exception(
                    f"Error while calling `{func_or_input.__name__}`")
                return self.make_response(self.err("Unexpected error"), 500)
            if isinstance(data, ContinueProcessing):
                params.update(data)
            else:
                return self.make_response(data, status)
        logger.error(f"Processing never stopped for `{type(self).__name__}`")
        return self.make_response(self.err("Dropoff"), 500)
