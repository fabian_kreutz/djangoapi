import pydantic
from typing import Optional
from django.views.generic import View
from django.http import JsonResponse, HttpResponseNotAllowed
from django.urls import path


class ContactObject(pydantic.BaseModel):
    name: Optional[str] = None
    url: Optional[str] = None
    email: Optional[str] = None


class LicenseObject(pydantic.BaseModel):
    name: Optional[str] = None
    url: Optional[str] = None


class ExternalDocumentation(pydantic.BaseModel):
    url: str
    description: Optional[str] = None


class PathItem(pydantic.BaseModel):
    description: Optional[str]
    summary: str
    operations: dict[str, type[View]]

    class Config:
        arbitrary_types_allowed = True

    def __init__(self, description=None, summary="", **kwargs):
        for op_name in kwargs.keys():
            if op_name not in View.http_method_names:
                raise ValueError(f"Not a HTTP method: {op_name}")
        super().__init__(
            description=description, summary=summary,
            operations=kwargs)

    def as_view(self, route):
        def wrapped(request, *args, **kwargs):
            if not (view_class := self.operations.get(request.method.lower())):
                return HttpResponseNotAllowed(list(self.operations.keys()))
            view = view_class(route=route)
            view.setup(request, *args, **kwargs)
            return view.dispatch(request, *args, **kwargs)
        wrapped.csrf_exempt = True
        return wrapped


class ApiEndpoint(pydantic.BaseModel):
    title: str
    version: str
    description: Optional[str] = None
    terms_of_service: Optional[str] = None
    contact: ContactObject | dict[str, str] | None = None
    license: LicenseObject | dict[str, str] | None = None
    external_docs: ExternalDocumentation | dict[str, str] | None = None
    paths: dict[str, PathItem] = pydantic.Field(default_factory=dict)
    add_version_endpoint = True

    @property
    def urls(self):
        urlpatterns = [
            path(url_path, item.as_view(url_path))
            for url_path, item in self.paths.items()]
        if self.add_version_endpoint:
            urlpatterns += [
                path("version/", self.version_view, name="version")]
        for pp in urlpatterns:
            pp.__source__ = self
        return urlpatterns

    def version_view(self, request):
        return JsonResponse({"title": self.title, "version": self.version})

    def with_prefix(self, path_prefix):
        return self.copy(
            deep=True,
            update={"paths": {
                path_prefix + route: data
                for route, data in self.paths.items()}})
