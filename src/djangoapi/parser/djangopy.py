import datetime
import inspect
from pydantic import BaseModel
import re
import types
import typing
from apigen.spec import OpenApiV3Spec
from apigen.exceptions import ApigenError
from apigen.parsers import BaseParser
from djangoapi.apibase import ApiEndpoint, ApiInput, ContinueProcessing


DEFAULT_HTTP_STATUS_MESSAGE = {
    100: "Continue",
    102: "Processing",
    200: "OK",
    201: "Created",
    202: "Accepted",
    203: "Non authoritative info",
    204: "No content",
    301: "Moved permanently",
    302: "Found",
    307: "Temporary redirect",
    308: "Permanent redirect",
    400: "Bad request",
    401: "Unauthorized",
    403: "Forbidden",
    404: "Not found",
    405: "Method not allowed",
    406: "Not acceptable",
    408: "Request timeout",
    409: "Conflict",
    410: "Gone",
    411: "Length required",
    415: "Unsupported media type",
    500: "Internal server error",
    501: "Not implemented",
    502: "Bad gateway",
    503: "Service unavailable",
    504: "Gateway timeout",
}
ROUTE_VARS_RE = re.compile(r'\{([a-z][\w]+)\}', re.I)


def default_message(status):
    return DEFAULT_HTTP_STATUS_MESSAGE.get(int(status), "?")


def filtered_return_annotation(return_type):
    if return_type is None or return_type == inspect._empty:
        return set()
    if typing.get_origin(return_type) in [types.UnionType, typing.Union]:
        results = typing.get_args(return_type)
    else:
        results = [return_type]
    return set(
        rt for rt in results
        if rt not in [typing.Any, ContinueProcessing])


def traverse_urlpatterns(pats, prefix):
    for elem in pats:
        if hasattr(elem, "url_patterns"):
            sub_prefix = prefix + str(elem.pattern)
            # The ApiEndpoint puts itself as __source__ into all paths
            if any(hasattr(e, "__source__") for e in elem.url_patterns):
                yield sub_prefix, elem.url_patterns[0].__source__
            else:
                yield from traverse_urlpatterns(elem.url_patterns, sub_prefix)


class PythonServerIntrospector(BaseParser):
    @classmethod
    def can_parse(cls, path):
        if path.suffix != ".py":
            return False
        for line in path.open():
            if "djangoapi" in line and "apibase" in line:
                return True
        return False

    def parse(self, path):
        exec(path.read_text(), globs := {})
        del globs["__builtins__"]
        if (pats := globs.get("urlpatterns")):
            api_endpoints = {
                prefix: endpoint.with_prefix(prefix)
                for prefix, endpoint in traverse_urlpatterns(pats, "/")}
        else:
            api_endpoints = {
                key: value
                for key, value in globs.items()
                if isinstance(value, ApiEndpoint)}
        if len(api_endpoints) == 0:
            raise ApigenError(
                f"No ApiEndpoint instance or urlpatterns found in {path}")
        elif len(api_endpoints) > 1:
            raise ApigenError(
                f"Found several ApiEndpoint instances in {path}: "
                f"{', '.join(api_endpoints.keys())}")
        return self.introspect_endpoint(list(api_endpoints.values())[0])

    def introspect_endpoint(self, api_endpoint: ApiEndpoint) -> OpenApiV3Spec:
        info_fields = [
            "title", "version", "description",
            "terms_of_service", "contact", "license"]
        data: dict[str, typing.Any] = {
            "api_version": "3.0.1",
            "info": {
                key: value
                for key, value in api_endpoint.dict().items()
                if value and key in info_fields},
            "externalDocs": api_endpoint.external_docs,
            "paths": {},
            "components": {"schemas": {}}}
        if (tos := data["info"].pop("terms_of_service", None)):
            data["info"]["termsOfService"] = tos
        self.schemas = data["components"]["schemas"]
        tags = {}
        for route, path in api_endpoint.paths.items():
            data["paths"][route] = pathitem = {
                method: self.introspect_operation(view, route)
                for method, view in path.operations.items()}
            for op in pathitem.values():
                if (op_tags := op.get("tags")):
                    op["tags"] = [t["name"] for t in op_tags]
                    tags.update({t["name"]: t for t in op_tags})
            pathitem.update({
                key: value
                for key, value in [
                    ("description", path.description),
                    ("summary", path.summary)]
                if value is not None})
        if tags:
            data["tags"] = list(tags.values())
        return OpenApiV3Spec(**data)

    def introspect_operation(self, view, route):
        route_vars = ROUTE_VARS_RE.findall(route)
        meta = getattr(view, "Meta", object())
        result = {"responses": {}, "parameters": []}
        if (tags := getattr(meta, "tags", [])):
            result["tags"] = [t.dict() for t in tags]
        if (opId := getattr(meta, "operation_id", None)):
            result["operationId"] = opId
        if (summary := getattr(meta, "summary", None)):
            result["summary"] = summary
        for func_or_input, status in view().get_pipeline():
            resp = result["responses"][status] = {
                "description":
                func_or_input.__doc__ or default_message(status),
                "return_values": set(),
            }
            if isinstance(func_or_input, ApiInput):
                if (params := func_or_input.query_data_cls):
                    result["parameters"].extend(
                        self.introspect_model(params, "query"))
                if (params := func_or_input.header_data_cls):
                    result["parameters"].extend(
                        self.introspect_model(params, "header"))
                if (params := func_or_input.body_data_cls):
                    schema = self.introspect_model(params, "body")
                    result["requestBody"] = {
                        "content": {"application/json": {"schema": schema}}}
            else:
                return_annotation = filtered_return_annotation(
                    inspect.signature(func_or_input).return_annotation)
                if status >= 400 and not return_annotation:
                    return_annotation = set([view.ErrorResponse])
                signature = inspect.signature(func_or_input).parameters
                result["parameters"].extend(
                    self.introspect_path_parameters(signature, route_vars))
                resp["return_values"] |= return_annotation
        for status, resp in result["responses"].items():
            rvs = list(resp.pop("return_values", []))
            if (200 <= status < 300 or 400 <= status) and rvs:
                schemas = [self.introspect_response_value(rr) for rr in rvs]
                if len(schemas) == 1:
                    schema = {"schema": schemas[0]}
                else:
                    schema = {"schema": {"oneOf": schemas}}
                resp["content"] = {"application/json": schema}
        return result

    def introspect_response_value(self, rv):
        if isinstance(rv, type) and issubclass(rv, BaseModel):
            result = self.introspect_model(rv, "body")
        else:
            result = self.get_schema_for_primitive_type(rv)
        return result

    def introspect_model(self, model, in_):
        model_schema = model.schema().copy()
        transform_dict(model_schema, self.remove_generated_titles)
        if (components := model_schema.pop("definitions", None)) is not None:
            self.schemas.update(components)
            transform_dict(model_schema, self.fix_component_references)
        self.add_object_defaults(model_schema, model)
        if in_ in ["query", "header"]:
            parameters = []
            for name, data in model_schema["properties"].items():
                parameters.append({
                    "name": name,
                    "in": in_,
                    "schema": data,
                    "required": name in model_schema.get("required", [])})
            return parameters
        else:
            self.remove_default_titles(model_schema)
            return model_schema

    def introspect_path_parameters(self, signature, route_vars):
        for name, param in signature.items():
            if name in ["self", "request"] or name.startswith("_"):
                continue
            if name in ["header", "query", "body"] and \
                    issubclass(param.annotation, BaseModel):
                continue
            if name not in route_vars:
                continue
            yield {
                "name": name,
                "required": True,
                "in": "path",
                "schema": self.get_schema_for_primitive_type(param.annotation)}

    def get_schema_for_primitive_type(self, ptype):
        schema = {
            "type": {
                float: "number",
                int: "integer",
                bool: "boolean"}.get(ptype, "string")}
        if ptype == datetime.datetime:
            schema["format"] = "date-time"
        elif ptype == datetime.date:
            schema["format"] = "date"
        return schema

    def remove_generated_titles(self, name, data):
        if data.get("title") in [name, name.replace("_", " ").title()]:
            del data["title"]

    def remove_default_titles(self, data):
        if data.get("title") in ["BodyInput", "HeaderInput", "QueryInput"]:
            del data["title"]

    def fix_component_references(self, name, data):
        if list(data.keys()) == ["$ref"]:
            data["$ref"] = data["$ref"].replace(
                "definitions", "components/schemas")

    def add_object_defaults(self, schema, model):
        for name, field in schema["properties"].items():
            self.add_object_field_defaults(field, model.__fields__[name])
        # Should descend into objects
        # Should also descend into object.additionalObjects/schema
        # Should also descend into array.items

    def add_object_field_defaults(self, schema, model):
        if model.default_factory:
            schema["default"] = model.default_factory()
            if schema["type"] == "string" and \
                    isinstance(schema["default"],
                               (datetime.datetime, datetime.date)):
                schema["default"] = schema["default"].isoformat()


def transform_dict(data, function):
    for key, values in data.items():
        if isinstance(values, dict):
            function(key, values)
            transform_dict(values, function)
