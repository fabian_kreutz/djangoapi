from dataclasses import dataclass, field
import textwrap
from typing import Union, Any
import black


class Reference(str):
    def __repr__(self):
        return self


@dataclass
class FunctionCall:
    func_name: str
    args: list[Any] = field(default_factory=list)
    kwargs: dict[str, Any] = field(default_factory=dict)

    def __repr__(self):
        return self.render()

    def render(self):
        result = self.func_name + "("
        options = [repr(arg) for arg in self.args]
        for key, value in self.kwargs.items():
            options += [f"{key}={value!r}"]
        return result + ", ".join(options) + ")"


@dataclass
class Assignment:
    name: str
    type_annotation: str = ""
    value: Any = ""

    def render(self):
        result = self.name
        if self.type_annotation:
            result += f": {self.type_annotation}"
        if self.value is not None:
            result += f" = {self.value!r}"
        return result


class ImportDefinitions(dict):
    def add(self, name) -> str:
        mod, cls = name.rsplit(".", 1)
        self.setdefault(mod, set()).add(cls)
        return cls

    def render(self):
        return [
            f"from {mod} import {', '.join(classes)}"
            for mod, classes in sorted(self.items())]


@dataclass
class MethodDefinition:
    name: str
    arguments: list[str]
    return_type: str | None
    docstring: str
    body: str

    def render(self):
        args = ["self"] + self.arguments
        signature = f"def {self.name}({', '.join(args)}):\n"
        if self.return_type:
            signature = signature[:-2] + f" -> {self.return_type}:\n"
        fbody = self.body or "pass"
        if self.docstring:
            fbody = f"'''{self.docstring}'''\n" + fbody
        return signature + textwrap.indent(fbody, " " * 4)


NamespaceItem = Union["ClassDefinition", Assignment, MethodDefinition]


class Namespace(list[NamespaceItem]):
    names: list[str]

    def unique_name(self, name):
        unique_name, ii = name, 0
        while unique_name in self.names:
            unique_name = f"{name}{ii}"
            ii += 1
        return unique_name

    def append(self, item):
        if item.name not in self.names:
            self.names.append(item.name)
            super().append(item)

    def render(self) -> str:
        return "\n".join(item.render() for item in self)


@dataclass
class ClassDefinition(Namespace):
    name: str
    superclasses: list[str]
    docstring: str = ""

    def __init__(self, name, superclasses=None, attributes=None):
        self.names = []
        self.name = name
        self.superclasses = superclasses or []
        for ii in (attributes or []):
            self.append(ii)

    def render(self) -> str:
        result = f"class {self.name}"
        if self.superclasses:
            result += f"({', '.join(self.superclasses)})"
        cls_body = super().render()
        if self.docstring:
            cls_body = f"'''{self.docstring}'''\n" + cls_body
        return result + ":\n" + textwrap.indent(cls_body, " " * 4)


class CodeFile(Namespace):
    imports: ImportDefinitions
    tags: list[str]
    api_definition: str

    def __init__(self):
        self.black_mode = black.Mode(line_length=79, preview=True)
        self.imports = ImportDefinitions()
        self.names = []

    def get_import(self, name):
        return self.imports.add(name)

    def render(self) -> str:
        result = "\n".join(self.imports.render())
        if result:
            result += "\n\n\n"
        result += super().render()
        return black.format_str(result, mode=self.black_mode)
