from apigen.spec.openapi import Format, SchemaObject
from .elements import Assignment, ClassDefinition, FunctionCall, Reference


PD_TO_JS = {
    "title": "title",
    "description": "description",
    "multiple_of": "multipleOf",
    "gt": "exclusiveMinimum",
    "ge": "minimum",
    "lt": "exclusiveMaximum",
    "le": "maximum",
    "min_items": "minItems",
    "max_items": "maxItems",
    "unique_items": "uniqueItems",
    "min_length": "minLength",
    "max_length": "maxLength",
    "regex": "pattern",
    "deprecated": "deprecated",
    "format": "format",
}


def slugify(text):
    return text\
        .translate({ord(x): " " for x in "-_/{}[]#$%,;.:"})\
        .title().replace(" ", "")


def get_mime_schema(obj):
    if (schema := getattr(obj, "schema_", None)) is not None:
        return schema
    if (mimetype := obj.content.get("application/json")):
        return mimetype.schema_
    return list(obj.content.values())[0].schema_


def get_primitive_type(cf, schema):
    match schema.type:
        case "string":
            match schema.format:
                case Format.string_date_time:
                    return cf.get_import("datetime.datetime")
                case Format.string_date:
                    return cf.get_import("datetime.date")
                case Format.string_binary:
                    return "bytes"
                case _:
                    return "str"
        case "integer":
            return "int"
        case "number":
            return "float"
        case "boolean":
            return "bool"
    return None


def get_object_subtype(cf, name, schema, location=None):
    add_props = schema.additionalProperties
    if not schema.properties and isinstance(add_props, SchemaObject):
        match add_props.type:
            case "array":
                ftype = get_array_subtype(cf, name, add_props.items)
            case "object":
                name_base = (add_props.title or name).title() + "Value"
                ftype = get_object_subtype(cf, name_base, add_props)
            case _:
                ftype = get_primitive_type(cf, add_props)
        return f"dict[str, {ftype}]"
    if schema.referenceName:
        location = cf
        name = schema.referenceName.split("/")[-1]
    (location or cf).append(cls := ClassDefinition(
        name=name,
        superclasses=[cf.get_import("pydantic.BaseModel")]))
    cls.append(ClassDefinition(
        name="Config", attributes=[Assignment(
            "extra", value="forbid" if add_props is False else "allow")]))
    for prop_name, prop in schema.properties.items():
        cls.append(make_field_assignment(
            cf, prop_name, prop_name in schema.required, prop))
    return cls.name


def get_array_subtype(cf, name, items):
    if (ptype := get_primitive_type(cf, items)) is not None:
        subtype = ptype
    elif items.type == "array":
        subtype = get_array_subtype(cf, name, items.items)
    else:
        name_base = (items.title or name or "Array").title() + "Item"
        subtype = get_object_subtype(cf, cf.unique_name(name_base), items)
    return f"list[{subtype}]"


def get_enum_subtype(cf, name, options):
    cf.append(cls := ClassDefinition(
        name=name,
        superclasses=[cf.get_import("enum.Enum")]))
    for val in options:
        cls.append(Assignment(slugify(val).lower(), value=val))
    return cls.name


def make_field_assignment(cf, name, required, schema) -> Assignment:
    args = [schema.default] if schema.default else []
    no_defaults = schema.dict()
    options = {
        key: value
        for key, schema_key in PD_TO_JS.items()
        if (value := no_defaults.get(schema_key)) is not None}
    match schema.type:
        case "array":
            if schema.default is not None:
                args = []
                if not schema.default:
                    factory = Reference("list")
                else:
                    factory = Reference(f"lambda: {schema.default!r}")
                options["default_factory"] = factory
            ftype = get_array_subtype(cf, name, schema.items)
        case "object":
            ftype = get_object_subtype(cf, cf.unique_name(name), schema)
        case _:
            ftype = get_primitive_type(cf, schema)
            if ftype in ["date", "datetime"]:
                options.pop("format", None)
                if args:
                    args = []
                    schema.default = None
                    options["default_factory"] = Reference(f"{ftype}.today")
    if schema.enum:
        ftype = get_enum_subtype(cf, cf.unique_name(name.title()), schema.enum)
    ftype_needing_annotation = ftype in ["date", "datetime"]
    if not required and schema.default is None:
        opt = cf.get_import("typing.Optional")
        ftype = f"{opt}[{ftype}]"
    if options and ftype_needing_annotation:
        annotated = cf.get_import("typing.Annotated")
        func = FunctionCall(cf.get_import("pydantic.Field"), kwargs=options)
        options = {}
        ftype = f"{annotated}[{ftype}, {func.render()}]"
    if options:
        return Assignment(name, ftype, FunctionCall(
            cf.get_import("pydantic.Field"),
            args=args, kwargs=options))
    return Assignment(name, ftype, schema.default)
