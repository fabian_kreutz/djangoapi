# Changelog

* Mark all API views as CSRF-exempt
* Fix default value parsing/rendering for strings of format date or datetime
* Use snake-case for attributes that are camelcase in OpenAPI
* Only variables in the route are considered path parameters
* Support parsing django's urls.py so that the full path can be in the spec
* Add gitlab CI
* Implement recognition of function return values as http responses
* Use pydantic models instead of django forms
* Supply default http status message as response description if \_\_doc\_\_ missing
* generator and parser support parameters and requestBody
  * Fix PathItem.name to be description as in the spec
* apibase implementation can serve requests
  * apigen moved to github EP
* apibase endpoint can produce urls
  * there is a default version/ endpoint for the api
* Basic model definitions for apibase
* Implement simple code generation from openapi spec
* Initial version
