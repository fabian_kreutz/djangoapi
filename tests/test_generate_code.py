from dataclasses import dataclass
import inspect
import json
import pytest
import typing
from unittest.mock import MagicMock
from apigen.spec import openapi
from djangoapi.generator import PythonServerGenerator
from djangoapi.apibase import ContinueProcessing


def make_min_spec():
    return openapi.OpenApiV3Spec(
        api_version="3.0.1",
        info={
            "title": "Test API",
            "version": "1.2.3"},
        paths={
            "/trigger_event": {
                "post": {
                    "responses": {
                        "200": {
                            "description": "Success"}}}}})


def render(spec):
    code = PythonServerGenerator().render(spec)
    try:
        globs = {}
        exec(code, globs)
        del globs["__builtins__"]
        print(code)
        return globs
    except Exception as exc:
        print(code)
        raise exc


def test_generates_minimal_example():
    spec = make_min_spec()
    result = render(spec)
    assert result["api"].title == "Test API"
    assert result["api"].version == "1.2.3"
    assert "TapiPostTriggerEvent" in result
    assert result["TapiPostTriggerEvent"].mro()[1].__name__ == "ApiOperation"


def test_defines_meta_information():
    spec = make_min_spec()
    post = spec.paths["/trigger_event"].post
    post.summary = "FOOBAR"
    post.description = "This is foo beyond any repair"
    post.externalDocs = openapi.ExternalDocumentationObject(
        url="https://docs.somewhere.com/api",
        description="Another really about medium long text")
    result = render(spec)
    opmeta = result["TapiPostTriggerEvent"].Meta
    assert opmeta.summary == post.summary
    assert opmeta.description == post.description
    assert opmeta.external_docs["url"] == post.externalDocs.url
    assert opmeta.operation_id == "TAPI-post-/trigger_event"


def test_uses_manually_added_operation_id():
    spec = make_min_spec()
    post = spec.paths["/trigger_event"].post
    post.operationId = "TEST-post-event_trigger[new]"
    result = render(spec)
    opmeta = result["TestPostEventTriggerNew"].Meta
    assert opmeta.operation_id == post.operationId


def test_defines_tags():
    spec = make_min_spec()
    sonnTag = openapi.TagObject(name="Sonn", description="Resting")
    spec.tags.append(sonnTag)
    spec.paths["/trigger_event"].post.tags = ["Sonn"]
    result = render(spec)
    assert (tag := result["sonnTag"]).description == "Resting"
    assert result["TapiPostTriggerEvent"].Meta.tags == [tag]


def test_generates_full_info_with_wrapping():
    spec = make_min_spec()
    spec.info.description = (
        "Considering that this is a markdown text "
        "with possibly [long links](https://some."
        "addresses/can/be_long.html).")
    spec.info.termsOfService = "Terms are shown"
    spec.info.contact = openapi.ContactObject(
        name="Bob Fleming", url="https://bbc.co.uk/folk/fleming",
        email="bob@bbc.co.uk")
    spec.info.license = openapi.LicenseObject(name="MIT")
    spec.externalDocs = openapi.ExternalDocumentationObject(
        url="https://docs.somewhere.com/api",
        description="Another really about medium long text")
    result = PythonServerGenerator().render(spec)
    assert 'terms_of_service="Terms are shown",' in result
    assert 'description=(\n        "Considering that' in result
    assert 'contact={\n        "name": "Bob Fleming",' in result
    assert 'license={"name": "MIT",' in result
    assert 'external_docs={\n        "url": "https://' in result


def test_asyncapi_spec_not_supported():
    from apigen.spec.asyncapi import AsyncApiV2Spec
    with pytest.raises(ValueError, match="only from OpenAPI"):
        render(AsyncApiV2Spec(
            api_version="2.4.0",
            info={"title": "Test API", "version": "1.2.3"}))


def test_description_in_paths():
    spec = make_min_spec()
    path = spec.paths["/trigger_event"]
    path.summary = "Path summary"
    path.description = "Path description"
    path_item = list(render(spec)["api"].paths.values())[0]
    assert path_item.summary == path.summary
    assert path_item.description == path.description


def test_datetime_with_default_value():
    spec = make_min_spec()
    resp = spec.paths["/trigger_event"].post.responses[200]
    resp.content = {"application/json": openapi.MediaTypeObject(
        schema={"type": "object", "properties": {"time": {
            "type": "string",
            "format": "date-time",
            "default": "2020-01-01T12:30:45Z"}}})}
    op_class = render(spec)["api"].paths["/trigger_event"].operations["post"]
    resp = op_class.ProcessResponse()
    assert resp.time is not None


def test_path_item_pipeline_sorted():
    spec = make_min_spec()
    spec.paths["/trigger_event"].post.responses.update({
        404: openapi.ResponseObject(description="Object not found"),
        402: openapi.ResponseObject(description="Payment Required"),
        501: openapi.ResponseObject(description="Not Implemented"),
        100: openapi.ResponseObject(description="Continue")})
    operation = render(spec)["TapiPostTriggerEvent"]
    f1, f2, f3, f4, f5 = operation._TapiPostTriggerEvent__pipeline
    assert f1[1] == 402
    assert f1[0].__name__ == "precondition"
    assert f1[0].__doc__ == "Payment Required"
    assert f2[1] == 404
    assert f2[0].__name__ == "get_object"
    assert f2[0].__doc__ == "Object not found"
    assert f3[1] == 100
    assert f3[0].__name__ == "handle"
    assert f3[0].__doc__ == "Continue"
    assert f4[1] == 200
    assert f4[0].__name__ == "process"
    assert f4[0].__doc__ == "Success"
    assert f5[1] == 501
    assert f5[0].__name__ == "on_error"
    assert f5[0].__doc__ == "Not Implemented"
    op_inst = operation()
    assert op_inst.process(None) == {"result": "Success"}
    assert isinstance(op_inst.precondition(None), ContinueProcessing)


def call_spec(method="get", path="/go/", **data):
    spec = openapi.OpenApiV3Spec(
        api_version="3.0.1",
        info={"title": "Test API", "version": "1.2.3"},
        paths={
            path: {
                method: {
                    **data,
                    "responses": {
                        "200": {"description": "Success"},
                        "400": {"description": "Invalid input data"}}}}})
    op_name = "Tapi" + method.capitalize() + "".join(
        elem.replace("{", "").replace("}", "").capitalize()
        for elem in path.split("/"))
    module = render(spec)
    op = module[op_name]
    pipe = getattr(op, f"_{op_name}__pipeline")
    pipe[1] = (mkp := MagicMock(wraps=pipe[1][0]), pipe[1][1])
    mkp.__name__ = "process"
    return op, mkp, module["api"].urls[0]


def three_param_get(request):
    op, mkp, url = call_spec(
        path="/user/{id}/",
        parameters=[
            {"name": "id", "in": "path",
             "schema": {"type": "integer"}},
            {"name": "version", "in": "header",
             "schema": {"type": "string",
                        "minLength": 3, "maxLength": 10}},
            {"name": "active", "in": "query",
             "schema": {"type": "boolean"}},
        ])
    resolver_match = url.resolve(path=request.path)
    if resolver_match is None:
        return {
            "call_args": None,
            "status_code": 404,
            "content": {"result": "error", "errors": "Path not found"}}
    response = op().dispatch(request=request, **resolver_match.kwargs)
    return {
        "call_args": mkp.call_args,
        "status_code": response.status_code,
        "content": json.loads(response.content.decode())}


def test_three_get_parameters_successful(rf):
    req = rf.get("/user/123/", data={"active": True}, HTTP_X_VERSION="5.4")
    result = three_param_get(req)
    process_params = result["call_args"].kwargs
    assert result["status_code"] == 200
    assert result["content"] == {"result": "Success"}
    assert "request" in process_params
    assert process_params["id"] == 123
    assert process_params["query"].active is True
    assert process_params["header"].version == "5.4"


@pytest.mark.parametrize("argsdiff,error", [
    ({"path": "/user/not-integer/"}, "Path not found"),
    ({"data": {"active": 123}},
     {"active": "value could not be parsed to a boolean"}),
    ({"HTTP_X_VERSION": "1"},
     {"version": "ensure this value has at least 3 characters"}),
    ({"HTTP_X_VERSION": "1234.567.890"},
     {"version": "ensure this value has at most 10 characters"}),
])
def test_three_parameter_failures(rf, argsdiff, error):
    parameters = {
        "path": "/user/123/",
        "data": {"active": True},
        "HTTP_X_VERSION": "5.4"}
    parameters.update(argsdiff)
    result = three_param_get(rf.get(**parameters))
    assert 400 <= result["status_code"] < 500
    assert result["content"]["result"] == "error"
    assert result["content"]["errors"] == error


@pytest.mark.parametrize("schema,data,error", [
    ({"type": "string", "minLength": 5}, {"name": "abc"},
     "ensure this value has at least 5 characters"),
    ({"type": "string", "maxLength": 5}, {"name": "abcdef"},
     "ensure this value has at most 5 characters"),
    ({"type": "string", "pattern": "[misp]+"}, {"name": "ohio"},
     'string does not match regex "[misp]+"'),
    ({"type": "integer", "minimum": 3}, {"name": 2},
     "ensure this value is greater than or equal to 3"),
    ({"type": "integer", "maximum": 3}, {"name": 4},
     "ensure this value is less than or equal to 3"),
    ({"type": "integer", "exclusiveMinimum": 3}, {"name": 3},
     "ensure this value is greater than 3"),
    ({"type": "integer", "exclusiveMaximum": 3}, {"name": 3},
     "ensure this value is less than 3"),
    ({"type": "integer", "multipleOf": 3}, {"name": 5},
     "ensure this value is a multiple of 3"),
    ({"type": "string", "format": "date"}, {"name": "yesterday"},
     "invalid date format"),
    ({"type": "string", "enum": ["Alice", "Bob", "Clive"]}, {"name": "Tom"},
     "value is not a valid enumeration member; permitted: "
     "'Alice', 'Bob', 'Clive'"),
    ])
def test_field_validation(rf, schema, data, error):
    op, _mkp, _url = call_spec(
        parameters=[
            {"name": "name", "in": "query",
             "schema": schema}])
    req = rf.get("/go/", data=data)
    response = op().dispatch(request=req)
    assert response.status_code == 400
    result = json.loads(response.content.decode())["errors"]
    assert result["name"] == error


def test_post_with_request_body(rf):
    op, mkp, _url = call_spec(
        method="post",
        requestBody={
            "content": {
                "application/json": {
                    "schema": {
                        "type": "object",
                        "properties": {
                            "name": {"type": "string"}}}}}})
    req = rf.post("/go/", data={"name": "Bob"})
    response = op().dispatch(request=req)
    assert response.status_code == 200
    assert mkp.call_args.kwargs["body"].name == "Bob"


def test_post_with_complex_request_body(rf):
    op, mkp, _url = call_spec(
        method="post",
        requestBody={
            "content": {
                "application/json": {
                    "schema": {
                        "type": "object",
                        "properties": {
                            "people": {"type": "array", "items": {
                                "type": "object", "properties": {
                                    "name": {"type": "string"}}}}}}}}})
    req = rf.post(
        "/go/", content_type="application/json",
        data={"people": [{"name": "Bob"}]})
    response = op().dispatch(request=req)
    assert response.status_code == 200
    assert [p.name for p in mkp.call_args.kwargs["body"].people] == ["Bob"]


def test_body_reference_points_to_external_class():
    spec = openapi.OpenApiV3Spec(
        api_version="3.0.1",
        info={"title": "Test API", "version": "1.2.3"},
        paths={
            "/go": {
                "post": {
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/MyBody"}}}},
                    "responses": {
                        "200": {
                            "description": "Success"}}}}},
        components={
            "schemas": {
                "MyBody": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "age": {"type": "integer"}}}}})
    result = render(spec)
    assert "MyBody" in result
    sig = inspect.signature(result["TapiPostGo"].process).parameters
    assert sig["body"].annotation == result["MyBody"]


def test_responds_415_without_request_body():
    spec = make_min_spec()
    spec.paths["/trigger_event"].post.responses.update({
        415: openapi.ResponseObject(description="Method not allowed")})
    operation = render(spec)["TapiPostTriggerEvent"]
    f415, _f200 = operation._TapiPostTriggerEvent__pipeline
    assert f415[0].__name__ == "supported_mediatypes"


@dataclass
class FakeRequest:
    content_type: str


def test_responds_415_with_request_body():
    spec = make_min_spec()
    spec.paths["/trigger_event"].post.responses.update({
        415: openapi.ResponseObject(description="Method not allowed")})
    spec.paths["/trigger_event"].post.requestBody = openapi.RequestBodyObject(
        content={"application/esoteric": {"schema": {"type": "string"}}})
    op = render(spec)["TapiPostTriggerEvent"]
    f415, _f200 = op._TapiPostTriggerEvent__pipeline
    assert isinstance(
        f415[0](op(), FakeRequest("application/esoteric")), ContinueProcessing)
    assert f415[0](op(), FakeRequest("application/json")).errors == {
        'content-type': 'Must be one of application/esoteric'}


@pytest.mark.parametrize("schema", [
    {"type": "object",
     "required": ["message"],
     "properties": {"message": {"type": "string"}}},
    {"type": "string"}])
def test_error_response_from_apiinput_in_other_func(schema):
    spec = make_min_spec()
    error_response = {"application/json": {"schema": schema}}
    spec.paths["/trigger_event"].post.responses.update({
        400: openapi.ResponseObject(
            description="Bad Input", content=error_response),
        404: openapi.ResponseObject(
            description="Not Found", content=error_response)})
    op = render(spec)["TapiPostTriggerEvent"]
    assert hasattr(op, "ErrorResponse")
    sig = inspect.signature(op.get_object).return_annotation
    assert op.ErrorResponse in typing.get_args(sig)
