import json
from pathlib import Path
import pytest
import os
import subprocess
from apigen.differ import SpecDiffer
from helpers.code import load_fixture, CallHelper
from djangoapi.parser import PythonServerIntrospector
from djangoapi.generator import PythonServerGenerator


@pytest.fixture
def ch(rf):
    return CallHelper(rf)


def is_good_python_code(file):
    bindir = Path(os.environ["VIRTUAL_ENV"]).relative_to(os.getcwd()) / "bin"
    ns = load_fixture(file)
    assert "api" in ns
    # No need to check by flake8 because code is piped through black
    cp = subprocess.run([f"{bindir}/mypy", file])
    return cp.returncode == 0


@pytest.mark.parametrize(
    "fixture",
    (Path(__file__).parent / "fixtures" / "apibase").glob("*.py"))
def test_fixture_api_is_parsable(fixture, tmp_path):
    spec1 = PythonServerIntrospector().parse(fixture)
    assert spec1.type == "openapi"
    json.dump(
        spec1.dict(), fixture.with_suffix(".json").open("wt"), indent="  ")

    generated_code_file = tmp_path / "api.py"
    code = PythonServerGenerator().render(spec1)
    generated_code_file.write_text(code)
    assert is_good_python_code(generated_code_file)

    spec2 = PythonServerIntrospector().parse(generated_code_file)
    differences = SpecDiffer().diff(spec1, spec2)
    assert differences == []


def test_simple_example_works(ch):
    ch.load_fixture("simple_example.py")
    result = ch.request("post")
    assert result.status_code == 415
    assert result.json == {
        "result": "error",
        "errors": {"content-type": "Must be one of application/json"}}

    ch.set_content_type("application/json")
    result = ch.request("post")
    assert result.status_code == 400
    assert result.json == {
        "result": "error",
        "errors": {"name": "field required", "url": "field required"}}

    ch.set_args(data={"name": "Bob", "url": "https://example.com"})
    result = ch.request("post")
    assert result.status_code == 201
    assert result.json == {
        "result": "success",
        "is_new": True}

    ch.set_args(data={"name": "Nobody", "url": "https://example.com"})
    result = ch.request("post")
    assert result.status_code == 500
    assert result.json == {
        "result": "error",
        "errors": {"processing": "Unexpected error"}}

    ch.set_args(data={"name": "Flop", "url": "https://example.com"})
    result = ch.request("post")
    assert result.status_code == 500
    assert result.json == {
        "result": "error",
        "errors": {"processing": "Dropoff"}}


def test_minimial_example_works(ch):
    ch.load_fixture("minimal_example.py")
    result = ch.request("get")
    assert result.status_code == 200
    assert result.content == b''


def test_complex_example_works(ch):
    ch.load_fixture("complex_example.py")
    result = ch.request("get")
    assert result.status_code == 401

    ch.set_args(header={"HTTP_AUTH": "admin"})
    result = ch.request("get")
    assert result.json == {
        "amount": 1,
        "result": "success",
        "ids": [{"name": "Bob", "url": "https://example.com"}]}

    result = ch.request("post")
    assert result.status_code == 415

    ch.set_content_type("application/json")
    ch.set_args(
        header={"HTTP_VERSION": 1},
        query={"page": 2},
        data={"name": "Nobody", "url": "https://foo.test"})
    result = ch.request("post")
    assert result.status_code == 404

    ch.args["data"]["name"] = "Flop"
    result = ch.request("post")
    assert result.status_code == 501
    assert result.json == {
        "result": "error",
        "errors": {"processing": "Giving up: Is a flop"},
        }

    ch.args["data"]["name"] = "Bob"
    result = ch.request("post")
    assert result.status_code == 201
    assert result.content == b''
