from djangoapi.codegen.elements import \
    CodeFile, Assignment, ClassDefinition, MethodDefinition, \
    FunctionCall, Reference


def test_simple_rendition():
    cf = CodeFile()
    cf.append(Assignment("greeting", None, "Hello"))
    cf.append(MethodDefinition(
        "main", ["name"], None, {}, "return f'{greeting} {name}!'"))
    exec(cf.render(), globs := {})
    assert globs["main"](None, "World") == "Hello World!"


def test_renders_beautifully_indented_code():
    cf = CodeFile()
    cf.append(Assignment(
        "some_evil_global_variable",
        "EsotericAndLongType",
        FunctionCall(
            "even_the_function_name_is_long",
            ["but look at the arguments"],
            {"eye": "has not seen", "ear": "has not heard"})))
    code = cf.render()
    max_line_length = max(len(line) for line in code.splitlines())
    assert max_line_length < 80


def test_handles_imports():
    cf = CodeFile()
    cls = ClassDefinition(
        "Smurf", [cf.get_import("pydantic.BaseModel")])
    cf.append(cls)
    cls.append(Assignment("name", "str"))
    exec(cf.render(), globs := {})
    assert "Smurf" in globs


def test_same_name_in_namespace_ignores():
    cf = CodeFile()
    cf.append(ClassDefinition(
        "Smurf", attributes=[Assignment("name", "str", None)]))
    cf.append(ClassDefinition(
        "Smurf", attributes=[Assignment("fame", "str", None)]))
    assert "fame" not in cf.render()


def test_supports_unique_name_creation():
    cf = CodeFile()
    cf.append(ClassDefinition(
        cf.unique_name("Smurf"),
        attributes=[Assignment("fame", "int", 3)]))
    cf.append(ClassDefinition(
        cf.unique_name("Smurf"),
        attributes=[Assignment("name", "str", "Papa Schlumpf")]))
    exec(cf.render(), globs := {})
    assert globs["Smurf"].fame == 3
    assert globs["Smurf0"].name == "Papa Schlumpf"


def test_enums_and_references():
    cf = CodeFile()
    enum = cf.get_import("enum.Enum")
    cf.append(ClassDefinition("Fame", [enum], [
        Assignment("NONE", value=0),
        Assignment("LOW", value=1),
        Assignment("MEDIUM", value=5),
        Assignment("HIGH", value=10)]))
    cf.append(ClassDefinition(
        cf.unique_name("Smurf"),
        attributes=[Assignment(
            "fame", value=Reference("Fame.NONE"))]))
    exec(cf.render(), globs := {})
    assert globs["Smurf"].fame.value == 0


def test_docstrings():
    cf = CodeFile()
    cf.append(cls := ClassDefinition("Smurf"))
    cls.docstring = "Blue tiny buggers"
    cls.append(MethodDefinition(
        "catchable", [], None, "Can it be catched?", "return False"))
    exec(cf.render(), globs := {})
    handy_smurf = globs["Smurf"]()
    assert handy_smurf.__doc__ == cls.docstring
    assert handy_smurf.catchable() is False
    assert handy_smurf.catchable.__doc__ == "Can it be catched?"
