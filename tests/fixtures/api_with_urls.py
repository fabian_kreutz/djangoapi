from django.urls import path, include
from djangoapi.apibase import ApiEndpoint, ApiOperation, PathItem


# Hide api definition from parser
def hidden():
    class Upload(ApiOperation):
        __pipeline = []

    api = ApiEndpoint(
        title="Test API",
        version="0.1",
        paths={
            "v2/upload/": PathItem(post=Upload)
        })
    return api.urls


def view_function(request):
    pass


imported_patterns = [
    path("test/", include(hidden()))
]
urlpatterns = [
    path("api/", include(imported_patterns)),
    path("home/", view_function),
]
