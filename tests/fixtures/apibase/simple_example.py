from typing import Union
from apigen.spec.openapi import TagObject
from djangoapi.apibase import \
    ApiEndpoint, ApiOperation, PathItem, ApiInput, ContinueProcessing
from djangoapi.apibase.helpers import accept
from pydantic import BaseModel, Field


fileTag = TagObject(
    name="Files",
    description="File related operations",
)


class UploadOperation(ApiOperation):
    class Meta:
        operation_id = "TST_upload-file"
        summary = "Summary of the operation"
        tags = [fileTag]

    class UploadRequest(BaseModel):
        class Config:
            extra = "forbid"

        name: str = Field(max_length=512)
        url: str = Field(format="url")

    class UploadResponse(BaseModel):
        class Config:
            extra = "forbid"

        is_new: bool
        result: str = "success"

    def process(self, request, body: UploadRequest)\
            -> Union[ContinueProcessing, UploadResponse]:
        if body.name == "Flop":
            return ContinueProcessing()
        elif body.name == "Nobody":
            raise Exception("There's nobody here")
        return self.UploadResponse(is_new=True)

    __pipeline = [
        (accept("application/json"), 415),
        (ApiInput(body=UploadRequest), 400),
        (process, 201),
    ]


api = ApiEndpoint(
    title="Example API",
    version="0.1",
    description="For testing purposes",
    contact={"name": "Tester"},
    externalDocs={
        "url": "https://example.test",
        "description": "Link to more info",
    },
    paths={
        "/upload/": PathItem(post=UploadOperation),
    },
)
