from typing import Union
from apigen.spec.openapi import TagObject
from djangoapi.apibase import \
    ApiEndpoint, ApiOperation, PathItem, ApiInput, ContinueProcessing
from djangoapi.apibase.helpers import accept
from pydantic import BaseModel, Field


fileTag = TagObject(
    name="Files",
    description="File related operations",
)


class OperationAuthBase(ApiOperation):

    def authenticate(self, request):
        if "Auth" in request.headers:
            return ContinueProcessing(user=request.headers["Auth"])
        return self.err("Unauthorized")

    def authorize(self, user, **kwargs):
        if user == "admin":
            return ContinueProcessing()
        return self.err("Permission denied")

    __pipeline = [
        (authenticate, 401),
        (authorize, 403),
    ]


class UploadObject(BaseModel):
    class Config:
        extra = "forbid"

    name: str = Field(max_length=512)
    url: str = Field(format="url")


class UploadStatus(OperationAuthBase):
    class Meta:
        operation_id = "TST_upload-status"
        tags = [fileTag]

    class UploadStatusResponse(BaseModel):
        class Config:
            extra = "forbid"

        amount: int
        ids: list[UploadObject]
        result: str = "success"

    def process(self, request, user) -> UploadStatusResponse:
        obj = UploadObject(name="Bob", url="https://example.com")
        return self.UploadStatusResponse(amount=1, ids=[obj])

    __pipeline = [
        (process, 200),
    ]


class UploadOperation(OperationAuthBase):
    class Meta:
        operation_id = "TST_upload-file"
        summary = "Summary of the operation"
        tags = [fileTag]

    class UploadHeader(BaseModel):
        version: int = 0

    class UploadQuery(BaseModel):
        page: int = 1

    def get_file(self, request, body, **kwargs):
        if body.name == "Nobody":
            return self.err(f"Unknown name: {body.name}")
        return ContinueProcessing()

    def process(self, request, body: UploadObject, **kwargs)\
            -> Union[ContinueProcessing, str]:
        if body.name == "Flop":
            return ContinueProcessing(error="Is a flop")
        return ""

    def giveup(self, request, error, **kwargs):
        return self.err(f"Giving up: {error}")

    __pipeline = [
        (accept("application/json"), 415),
        (ApiInput(header=UploadHeader, query=UploadQuery, body=UploadObject),
         400),
        (get_file, 404),
        (process, 201),
        (giveup, 501),
    ]


api = ApiEndpoint(
    title="Example API",
    version="0.1",
    description="For testing purposes",
    contact={"name": "Tester"},
    externalDocs={
        "url": "https://example.test",
        "description": "Link to more info",
    },
    paths={
        "/upload/": PathItem(get=UploadStatus, post=UploadOperation),
    },
)
