from pydantic import BaseModel
from djangoapi.apibase import \
    ApiEndpoint, ApiOperation, PathItem, ContinueProcessing


class PingOperation(ApiOperation):
    class Meta:
        operation_id = 'EAPI-get-/ping/'

    class ErrorResponse(BaseModel):
        message: str

    class OtherError(BaseModel):
        code: int

    class PingResult(BaseModel):
        interval: float

    def authenticate(self, request) -> ContinueProcessing | OtherError:
        return ContinueProcessing()

    def authorize(self, request) -> ErrorResponse | str | ContinueProcessing:
        return ContinueProcessing()

    def get_object(self, request):
        return ContinueProcessing()

    def process(self, request) -> PingResult | int:
        return 3

    __pipeline = [
        (authenticate, 401),
        (authorize, 403),
        (get_object, 404),
        (process, 200),
    ]


api = ApiEndpoint(
    title="Example API",
    version="0.1",
    paths={
        "/ping/": PathItem(get=PingOperation),
    },
)
