from djangoapi.apibase import \
    ApiEndpoint, ApiOperation, PathItem, ContinueProcessing


class PingOperation(ApiOperation):
    class Meta:
        operation_id = 'EAPI-get-/ping/'

    ErrorResponse = str

    def get_object(self, request):
        return ContinueProcessing()

    def process(self, request):
        pass

    __pipeline = [
        (get_object, 404),
        (process, 200),
    ]


api = ApiEndpoint(
    title="Example API",
    version="0.1",
    paths={
        "/ping/": PathItem(get=PingOperation),
    },
)
