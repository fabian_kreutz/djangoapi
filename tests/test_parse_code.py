from datetime import datetime
from pathlib import Path
import pytest
from djangoapi.parser.djangopy import PythonServerIntrospector
from apigen.differ import SpecDiffer
from apigen.exceptions import ApigenError
from apigen.spec.openapi import OpenApiV3Spec
from helpers.code import CodeHelper


def make_spec(info=None, path=None, responses=None, main=None, route="/go"):
    if responses is None:
        responses = {"200": {"description": "Success"}}
    return OpenApiV3Spec(
        api_version="3.0.1",
        info={"title": "Test API", "version": "1.2.3", **(info or {})},
        **(main or {}),
        paths={route: {
            **(path or {}),
            "get": {
                "operationId": "TAPI-get-/go",  # considered de-facto required
                "responses": responses}}})


@pytest.fixture
def ch(tmp_path):
    return CodeHelper(tmp_path)


def test_code_without_apibase_mention_not_parsable(ch):
    assert ch.can_parse('print("Hello World")\n') is False


def test_non_python_files_cannot_be_parsed(ch):
    assert ch.can_parse(make_spec(), name="api.spec") is False


def test_invalid_python_fails(ch):
    with pytest.raises(SyntaxError):
        ch.run("Sure djangoapi.apibase is mentioned, but this is not python")


def test_code_without_apiendpoint_instance_or_urls_fails(ch):
    with pytest.raises(
            ApigenError,
            match="No ApiEndpoint instance or urlpatterns found in "):
        ch.run('import djangoapi.apibase')


def test_minimal_apiendpoint_example_parsed(ch):
    spec = ch.run(make_spec())
    assert spec.type == "openapi"
    assert spec.api_version == "3.0.1"
    assert spec.info.title == "Test API"
    assert spec.info.version == "1.2.3"
    assert list(spec.paths.keys()) == ["/go"]
    method, operation = list(spec.paths["/go"].operations.items())[0]
    assert method == "get"
    assert operation.responses[200].description == "Success"


@pytest.mark.parametrize("expectation", [
    make_spec(info={        # 0
        "description": "Info desk",
        "termsOfService": "https://terms.com",
        "contact": {"name": "Bob"},
        "license": {"name": "MIT", "url": "https://mit.license"}}),
    make_spec(info={        # 1
        "contact": {
            "name": "Bob",
            "url": "https://bob.bbc.co.uk",
            "email": "bob.fleming@bbc.co.uk"}}),
    make_spec(path={        # 2
        "description": "* Go for it", "summary": "GO operations"}),
    make_spec(responses={   # 3
        "401": {
            "description": "Authentication required",
            "content": {"application/json": {"schema": {"type": "string"}}}}}),
    make_spec(              # 4
        main={"tags": [{"name": "Sonn", "description": "rest"}]},
        path={
            "summary": "Operation",
            "post": {
                "operationId": "necessary",
                "summary": "PostOperation",
                "responses": {"200": {"description": "Success"}},
                "tags": ["Sonn"]}}),
    make_spec(              # 5
        route="/go/{id}/",
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "parameters": [
                    {"name": "id", "in": "path",
                     "required": True,
                     "schema": {"type": "string"}},
                    {"name": "user_id", "in": "query",
                     "schema": {
                         "type": "string", "minLength": 23, "maxLength": 64}},
                    {"name": "date", "in": "query",
                     "schema": {
                         "type": "string", "format": "date",
                         "title": "Day of inception"}},
                    {"name": "time", "in": "query",
                     "schema": {"type": "string", "format": "date-time"}},
                    {"name": "data", "in": "query",
                     "schema": {"type": "string", "format": "binary"}},
                    {"name": "decimal", "in": "query",
                     "schema": {"type": "number"}},
                    {"name": "active", "in": "query",
                     "schema": {"type": "boolean", "default": True}},
                    {"name": "filtering", "in": "query",
                     "schema": {
                         "type": "string", "format": "regex",
                         "pattern": "abc\\d+"}},
                    {"name": "version", "in": "header",
                     "schema": {
                         "type": "integer", "minimum": 2, "maximum": 6}},
                ],
                "responses": {
                    "200": {"description": "Success"},
                    "400": {"description": "Invalid request"}}}}),
    make_spec(              # 6
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "active": {
                                        "type": "boolean", "default": True},
                                }}}}},
                "responses": {
                    "400": {"description": "Invalid request"}}}}),
    make_spec(              # 7
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "required": ["active"],
                                "properties": {
                                    "active": {"type": "boolean"},
                                    "inactive": {"type": "boolean"},
                                }}}}},
                "responses": {
                    "400": {"description": "Invalid request"}}}}),
    make_spec(              # 8
        route="/go/{day}/{time}/",
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "parameters": [
                    {"name": "time", "in": "path", "required": True,
                     "schema": {"type": "string", "format": "date-time"}},
                    {"name": "day", "in": "path", "required": True,
                     "schema": {"type": "string", "format": "date"}},
                ],
                "responses": {
                    "200": {"description": "Success"}}}}),
    make_spec(              # 9
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "parameters": [
                    {"name": "data", "in": "query",
                     "schema": {"type": "array", "items": {
                         "type": "array", "items": {"type": "string"}}}},
                ],
                "responses": {
                    "400": {"description": "Invalid request"}}}}),
    make_spec(              # 10
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "parameters": [
                    {"name": "data1", "in": "query", "required": True,
                     "schema": {"type": "object", "additionalProperties": {
                         "type": "string"}}},
                    {"name": "data2", "in": "query", "required": True,
                     "schema": {"type": "object", "additionalProperties": {
                         "type": "array", "items": {"type": "string"}}}},
                ],
                "responses": {
                    "400": {"description": "Invalid request"}}}}),
])
def test_roundtrip(ch, expectation):
    spec = ch.run(expectation)
    messages = SpecDiffer().diff(spec, expectation)
    assert not messages


def test_parses_api_from_urls():
    fixture = Path("tests", "fixtures", "api_with_urls.py")
    spec = PythonServerIntrospector().parse(fixture)
    assert list(spec.paths.keys()) == ["/api/test/v2/upload/"]


def test_recognizes_path_parameters_correctly(tmp_path):
    code_file = tmp_path / "api.py"
    code_file.write_text(
        'from djangoapi import apibase\n'
        '\n'
        'class Jet(apibase.ApiOperation):\n'
        '    def get_object(self, request, userId: int):\n'
        '        return {"greeting": f"Hello {userId}"}\n'
        '    def process(self, request, greeting: str, **_):\n'
        '         pass\n'
        '    __pipeline = [(get_object, 404), (process, 200)]\n'
        '\n'
        'api = apibase.ApiEndpoint(\n'
        '    title="T1", version="1",\n'
        '    paths={"go/{userId}/": apibase.PathItem(get=Jet)})\n')
    spec = PythonServerIntrospector().parse(code_file)
    params = spec.paths["go/{userId}/"].get.parameters
    assert len(params) == 1
    assert params[0].name == "userId"
    assert params[0].in_.name == "path"
    assert params[0].schema_.type == "integer"


def test_casts_datetime_default(tmp_path):
    code_file = tmp_path / "api.py"
    code_file.write_text(
        'from djangoapi import apibase\n'
        'from datetime import datetime\n'
        'from pydantic import BaseModel, Field\n'
        '\n'
        'class Jet(apibase.ApiOperation):\n'
        '    class JetResponse(BaseModel):\n'
        '       time: datetime = Field(default_factory=datetime.now)\n'
        '    def process(self, request) -> JetResponse:\n'
        '         pass\n'
        '    __pipeline = [(process, 200)]\n'
        '\n'
        'api = apibase.ApiEndpoint(\n'
        '    title="T1", version="1",\n'
        '    paths={"go/": apibase.PathItem(get=Jet)})\n')
    spec = PythonServerIntrospector().parse(code_file)
    prop = spec.paths["go/"].get.responses[200].content["application/json"].\
        schema_.properties["time"]
    default = datetime.fromisoformat(prop.default)
    assert (default - datetime.now()).total_seconds() < 2


def test_fails_on_multiple_apis(tmp_path):
    code_file = tmp_path / "api.py"
    code_file.write_text(
        'from djangoapi import apibase\n'
        '\n'
        'api1 = apibase.ApiEndpoint(title="T1", version="1", paths={})\n'
        'api2 = apibase.ApiEndpoint(title="T2", version="1", paths={})\n')
    with pytest.raises(
            ApigenError,
            match="Found several ApiEndpoint instances.*api1, api2"):
        PythonServerIntrospector().parse(code_file)


def test_does_not_crash_on_missing_operation_id(tmp_path):
    code_file = tmp_path / "api.py"
    code_file.write_text(
        'from djangoapi import apibase\n'
        '\n'
        'class Jet(apibase.ApiOperation):\n'
        '    def process(self, request):\n'
        '         """Success"""\n'
        '         pass\n'
        '    __pipeline = [(process, 200)]\n'
        '\n'
        'api = apibase.ApiEndpoint(\n'
        '    title="T1", version="1",\n'
        '    paths={"go/": apibase.PathItem(get=Jet)})\n')
    spec = PythonServerIntrospector().parse(code_file)
    assert spec.paths["go/"].get.operationId is None


def test_supplies_default_http_message(tmp_path):
    code_file = tmp_path / "api.py"
    code_file.write_text(
        'from djangoapi import apibase\n'
        '\n'
        'class Jet(apibase.ApiOperation):\n'
        '    def process(self, request):\n'
        '         pass\n'
        '    __pipeline = [(process, 200)]\n'
        '\n'
        'api = apibase.ApiEndpoint(\n'
        '    title="T1", version="1",\n'
        '    paths={"go/": apibase.PathItem(get=Jet)})\n')
    spec = PythonServerIntrospector().parse(code_file)
    assert spec.paths["go/"].get.responses[200].description == "OK"


def test_complex_root_level_models_go_into_components(ch):
    expectation = make_spec(
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "parameters": [
                    {"name": "data", "in": "query", "required": True,
                     "schema": {"type": "object", "additionalProperties": {
                         "type": "object", "properties": {"name": {
                             "type": "string"}}}}},
                ],
                "responses": {
                    "400": {"description": "Invalid request"}}}})
    spec = ch.run(expectation)
    messages = SpecDiffer().diff(spec, expectation)
    assert messages == []


def test_array_with_default_value(ch):
    expectation = make_spec(
        path={
            "post": {
                "operationId": "TAPI-post-/go",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "title": "PostRequest",
                                "properties": {
                                    "data1": {
                                        "type": "array",
                                        "items": {"type": "string"},
                                        "default": []},
                                    "data2": {
                                        "type": "array",
                                        "items": {"type": "integer"},
                                        "default": [1, 2, 3]}}}}}},
                "responses": {
                    "400": {"description": "Invalid request"}}}})
    spec = ch.run(expectation)
    messages = SpecDiffer().diff(spec, expectation)
    assert messages == []
    assert spec.paths["/go"].post.requestBody.content["application/json"]\
        .schema_.properties["data1"].default == []
