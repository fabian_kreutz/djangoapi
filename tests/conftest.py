import pytest
from helpers.logging import LogBucket


@pytest.fixture
def lg(caplog):
    return LogBucket(caplog)
