from djangoapi.version import version


def test_has_a_version():
    assert version is not None
