SECRET_KEY = 'django-test-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
DEBUG = True
ALLOWED_HOSTS = ["localhost"]
USE_TZ = True
DEFAULT_CHARSET = "UTF-8"
