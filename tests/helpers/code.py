import json
from pathlib import Path
from apigen.spec import OpenApiV3Spec
from djangoapi.generator.djangopy import PythonServerGenerator
from djangoapi.parser.djangopy import PythonServerIntrospector


class CodeHelper:
    def __init__(self, tmp_path):
        self.tmp_path = tmp_path

    def can_parse(self, code, name="api.py"):
        code_file = self.tmp_path / name
        if isinstance(code, OpenApiV3Spec):
            code = PythonServerGenerator().render(code)
        code_file.write_text(code)
        psi = PythonServerIntrospector()
        return psi.can_parse(code_file)

    def run(self, code):
        code_file = self.tmp_path / "api.py"
        if isinstance(code, OpenApiV3Spec):
            code = PythonServerGenerator().render(code)
        print(code)
        code_file.write_text(code)
        psi = PythonServerIntrospector()
        assert psi.can_parse(code_file)
        return psi.parse(code_file)


def load_fixture(fixture):
    try:
        exec(fixture.read_text(), ns := {})
    except Exception as exc:
        raise AssertionError(f"Unable to compile {fixture.name}: {exc}")
    return ns


class CallHelper:
    def __init__(self, rf):
        self.rf = rf
        self.api = None
        self.url_path = None
        self.args = {}
        self.query = {}

    def load_fixture(self, name):
        fixture_path = \
            Path(__file__).parent.parent / "fixtures" / "apibase" / name
        self.api = load_fixture(fixture_path)["api"]

    def set_path(self, path):
        self.url_path = path

    def set_content_type(self, ct):
        self.args["content_type"] = ct

    def set_args(self, data=None, header=None, query=None):
        if data:
            self.args["data"] = data
        if header:
            self.args.update(header)
        self.query = query

    def request(self, op):
        if self.url_path:
            item = self.api.paths[self.url_path]
        else:
            self.url_path, item = list(self.api.paths.items())[0]
        view = item.as_view(self.url_path)
        query = ""
        if self.query:
            query = "?" + "&".join(
                f"{key}={value}" for key, value in self.query.items())
        req = getattr(self.rf, op)(self.url_path + query, **self.args)
        if op in ["post", "put"] and not getattr(view, "csrf_exempt", False) \
                and "csrf_token" not in self.args.get("data", {}):
            assert False, "CSRF failure"
        result = view(req)
        if result.content.startswith(b"{"):
            result.json = json.loads(result.content)
        return result
