import enum
import json
import pytest
from pydantic import BaseModel
from django.http import JsonResponse
from django.views.generic.base import RedirectView
from djangoapi.apibase import \
    PathItem, ApiOperation, ApiEndpoint, ApiInput, ContinueProcessing
from djangoapi.apibase.helpers import accept


def test_path_items_take_only_http_operation_parameters():
    with pytest.raises(ValueError, match="Not a HTTP method: swing"):
        PathItem(swing=ApiOperation)


def test_path_items_take_only_views_as_operations():
    with pytest.raises(ValueError, match="subclass of View expected"):
        PathItem(get=PathItem)


def test_endpoint_can_produce_urls_with_version():
    ep = ApiEndpoint(
        title="Test API", version="1.2.3",
        paths={"/trigger_event": PathItem(get=ApiOperation)})
    trigger, version = ep.urls
    assert trigger.pattern.regex.pattern == r"^/trigger_event\Z"
    assert version.pattern.regex.pattern == r"^version/\Z"
    assert version.name == "version"
    assert json.loads(version.callback(request=None).content.decode()) == {
        "title": ep.title,
        "version": ep.version}


def test_endpoint_can_produce_urls():
    ep = ApiEndpoint(
        add_version_endpoint=False,
        title="Test API", version="1.2.3",
        paths={"/trigger_event": PathItem(get=ApiOperation)})
    assert len(ep.urls) == 1


def rq(httpop, operation, **kwargs):
    ep = ApiEndpoint(
        title="Test API", version="1.2.3",
        paths={"/go": PathItem(get=operation, post=operation)})
    resp = ep.urls[0].callback(request=httpop("/go", **kwargs))
    data = resp.content.decode()
    if data and data[0] == "{":
        data = json.loads(data)
    return data, resp.status_code


def test_endpoint_can_call_any_view(rf):
    assert rq(rf.get, RedirectView)[1] == 410


def test_endpoint_returns_405_on_undefined_method(rf):
    ep = ApiEndpoint(
        title="Test API", version="1.2.3",
        paths={"/go": PathItem(post=RedirectView)})
    assert ep.urls[0].callback(request=rf.get("/go")).status_code == 405


def test_operation_inherits_pipeline():
    class AuthOp(ApiOperation):
        def authenticate(self):
            pass
        __pipeline = [(authenticate, 401)]

    class InterOp(AuthOp):
        pass

    class TestOp(InterOp):
        def process(self):
            pass
        __pipeline = [(process, 200)]

    op = TestOp(request=None)
    assert [func.__name__ for func, _code in op.get_pipeline()] == [
        "authenticate", "process"]


def test_operation_process_with_dict_response(rf):
    class TestOp(ApiOperation):
        def authenticate(self, request):
            return ContinueProcessing(user="Bob")

        def process(self, request, user):
            return {"message": f"Hello {user}"}
        __pipeline = [(authenticate, 401), (process, 200)]

    assert rq(rf.get, TestOp) == ({"message": "Hello Bob"}, 200)


def test_operation_crash_returns_500(rf, caplog):
    class TestOp(ApiOperation):
        def failer(self, request):
            raise ValueError("Choking")
        __pipeline = [(failer, 200)]

    resp, status = rq(rf.get, TestOp)
    assert status == 500, resp
    assert resp["errors"] == {"processing": "Unexpected error"}
    assert caplog.records[0].message == "Error while calling `failer`"


def test_operation_dropoff_returns_500(rf, caplog):
    class TestOp(ApiOperation):
        def ignorer(self, request):
            return ContinueProcessing()
        __pipeline = [(ignorer, 200)]

    resp, status = rq(rf.get, TestOp)
    assert status == 500, resp
    assert resp["errors"] == {"processing": "Dropoff"}
    assert caplog.records[0].message == \
        "Processing never stopped for `TestOp`"


class Color(enum.Enum):
    blue = "B"
    white = "W"


def op_with_input(rf, method="get", ct="application/json", data=None):
    class InputForm(BaseModel):
        color: Color
        name: str = "Papa Schlumpf"

    if method == "get":
        input_pipe = [
            (ApiInput(query=InputForm), 400)]
    else:
        input_pipe = [
            (accept("application/json"), 415),
            (ApiInput(body=InputForm), 400)]

    class TestOp(ApiOperation):
        def process(self, request, **data):
            return {name: json.loads(val.json())
                    for name, val in data.items()}
        __pipeline = input_pipe + [(process, 200)]

    ep = ApiEndpoint(
        title="Test API", version="1.2.3",
        paths={"/go": PathItem(**{method: TestOp})})
    params = {}
    if method == "post":
        params["content_type"] = ct
    request = getattr(rf, method)("/go", data=data, **params)
    resp = ep.urls[0].callback(request)
    return json.loads(resp.content.decode()), resp.status_code


def test_input_form_with_invalid_input(rf):
    resp, status = op_with_input(rf)
    assert status == 400, resp
    assert resp["errors"] == {"color": "field required"}


def test_input_form_with_default_input(rf):
    resp, status = op_with_input(rf, data={"color": "W"})
    assert status == 200, resp
    assert resp == {'query': {'name': 'Papa Schlumpf', 'color': 'W'}}


def test_input_form_with_post_data(rf):
    resp, status = op_with_input(rf, method="post", data={"color": "B"})
    assert status == 200, resp
    assert resp == {'body': {'name': 'Papa Schlumpf', 'color': 'B'}}


def test_inacceptable_content_type(rf):
    resp, status = op_with_input(rf, method="post", ct="application/word-doc")
    assert status == 415, resp
    assert resp["errors"] == {
        "content-type": "Must be one of application/json"}


def test_input_form_with_bad_json(rf):
    resp, status = op_with_input(rf, method="post", data="not json")
    assert status == 400, resp
    assert "Expecting value: line 1" in resp["errors"]["_body"]


@pytest.mark.parametrize("method", ["get", "post"])
def test_supports_list_parameter_in_query(rf, method):
    class InputForm(BaseModel):
        brand: list[str]

    opts = {"query": InputForm} if method == "get" else {"body": InputForm}

    class TestOp(ApiOperation):
        def process(self, request, **data):
            return next(iter(data.values())).dict()
        __pipeline = [(ApiInput(**opts), 400), (process, 200)]

    data, status = rq(
        getattr(rf, method), TestOp, data={"brand": ["Toyota", "VW"]})
    assert status == 200, data
    assert data == {"brand": ["Toyota", "VW"]}


def test_get_ignores_extra_parameters(rf):
    resp, status = op_with_input(rf, data={"color": "W", "gender": "F"})
    assert status == 200, resp
    assert resp == {'query': {'color': 'W', 'name': 'Papa Schlumpf'}}


def test_input_from_header(rf):
    class InputForm(BaseModel):
        version: str

    class TestOp(ApiOperation):
        def process(self, request, header):
            return header.dict()
        __pipeline = [(ApiInput(header=InputForm), 400), (process, 200)]

    data, status = rq(
        rf.get, TestOp, **{"HTTP_X_VERSION": 3, "HTTP_X_ANY": "ignored"})
    assert status == 200, data
    assert data == {"version": "3"}


def test_post_does_not_ignore_extra_parameters(rf):
    class InputForm(BaseModel):
        name: str

        class Config:
            extra = "forbid"

    class TestOp(ApiOperation):
        __pipeline = [(ApiInput(body=InputForm), 400)]

    data, status = rq(
        rf.post, TestOp, data={"name": "Bob", "other": "value"})
    assert status == 400, data
    assert data["errors"] == {'other': 'extra fields not permitted'}


def test_body_input_can_have_nested_errors(rf):
    class NestedModel(BaseModel):
        name: str

        class Config:
            extra = "forbid"

    class ParentModel(BaseModel):
        child1: NestedModel
        child2: NestedModel
        childn: list[NestedModel]

    class TestOp(ApiOperation):
        __pipeline = [(ApiInput(body=ParentModel), 400)]

    data, status = rq(
        rf.post, TestOp,
        data=json.dumps({
            "child1": {"name": "Bob"},
            "child2": {"hobby": "fishing"},
            "childn": [{"name": "Alice"}, {}]}),
        content_type="application/json")
    assert status == 400, data
    assert data["errors"] == {
        'child2': {
            'name': 'field required',
            'hobby': 'extra fields not permitted'},
        'childn': {'1': {'name': 'field required'}}}


def test_error_can_be_simple_string(rf):
    class TestOp(ApiOperation):
        class ErrorResponse(str):
            pass

        def make_response(self, data, status):
            return JsonResponse(data, safe=False, status=status)

        __pipeline = [(accept("application/esoteric"), 415)]

    data, status = rq(rf.get, TestOp)
    assert status == 415
    assert data == \
        "\"{'content-type': 'Must be one of application/esoteric'}\""


def test_unknown_content_type_without_prior_check_is_500(rf, caplog):
    class InputForm(BaseModel):
        name: str

    class TestOp(ApiOperation):
        __pipeline = [(ApiInput(body=InputForm), 400)]

    _d, status = rq(
        rf.post, TestOp, content_type="application/ms-word", data="-")
    assert status == 500
    err = caplog.records[0]
    assert "Error while calling `InputForm`" in err.message
    assert "Unsupported content-type: `application/ms-word`" in err.exc_text


def test_empty_apiinput_still_has_name():
    assert ApiInput().__name__ == "Unnamed ApiInput"
