# Django API framework

This project helps implementing an OpenAPI validated API server.

## Documentation

See [main documentation](documentation/index.md).

## Testing or standalone usage

Use [tox](https://tox.wiki/en/latest/):

```sh
$ pip install tox
$ tox

# Generate server stub implementation from spec
$ .tox/py3/bin/apigen generate spec.json -l djangopy -o api.py

# Generate spec from server implementation
$ .tox/py3/bin/apigen generate api.py -l spec -o spec.json

# Validate specification against implementation
$ .tox/py3/bin/apigen validate spec.json api.py
```

## TODO

* Support return-value annotation for process function
* Path parameters are identified as parameters to the 200-process function
  * but those include extra values from preprocessing steps
* Make django optional?
* Integrate endpoint that publishes the version in generated/parsed specification
* Support non-object input bodies
